Throughout this tutorial, we will run some python scripts using Jupyter 
Notebook.The website 
https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/index.html 
shows how to install it and to start it in the different Operational Systems 
(Linux/Mac OS, Windows). Additionally, some Python packages are required: Numpy,
IPython, Matplotlib, and scikit-image. 

If you do not have such packages installed, I recommend the installation via 
pip install:

pip3 install --user numpy, ipython, matplotlib, scikit-image

You can either download the files from this Gitlab repository and copy them in 
a folder of your choice or, if you prefer, you can clone it by typing:

git clone https://gitlab.esrf.fr/jdasilva/tutorialptychocxdi2019.git

Once the files are in your computer, you can open Jupyter Notebook by:
- Windows: click on the Jupyter Notebook icon installed in the start menu.
- Linux/Mac OS: open the terminal and type jupyter notebook at the prompt.

There three notebooks there, which we will run in this order: 
1) Oversampling.ipynb
2) CDI_ER_HIO.ipynb 
3) PIE_ptycho.ipynb

It is important to mention that the Python codes in notebook 1 and 2 are 
inspired by the "Tutorial in Diffraction Imaging" by Gösta Huldt and Filipe Maia
(originally in MATLAB), which was available until recenlty at:
http://xray.bmc.uu.se/~ekeberg/molbiofys/tutorial.pdf 

Modifications have been made on the original code for educational reasons and 
Python compatibility. 

The Python implementation in notebook 3 is inspired by 
the MATLAB code available in the Appendix A of the Diploma thesis by 
Dr. Martin Dierolf, which is available here: 
https://www.psi.ch/sls/csaxs/PublicationsEN/thesis_dierolf.pdf

The present notebooks are intended for educational reasons only and for use 
during the Tutorial T.3 of the ESRF user meeting 2019. They are not be perfect, 
but keep been gradually improved. Apologies for possible mistakes and missing 
references. 

Additionally, the codes in the Python notebook are not intended to 
be efficient and should not be used for goals other than educational. There are
other more efficient and complete codes available, such as:
- Ptypy ( http://ptycho.github.io/ptypy/ )
- PyNX ( www.esrf.eu/computing/scientific/PyNX/README.html ).
